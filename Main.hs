{-# LANGUAGE PatternSynonyms #-}

import           Data.Bits ((.|.))
import           Data.IORef (IORef, newIORef, readIORef, writeIORef)
import           Foreign.Hoppy.Runtime (toGc, withScopedPtr)
import qualified Graphics.UI.Qtah.Core.QCoreApplication as QCoreApplication
import qualified Graphics.UI.Qtah.Core.QEvent as QEvent
import qualified Graphics.UI.Qtah.Core.Types as Qt
import           Graphics.UI.Qtah.Event (onEvent)
import           Graphics.UI.Qtah.Gui.QFont (QFont)
import qualified Graphics.UI.Qtah.Gui.QFont as QFont
import           Graphics.UI.Qtah.Gui.QKeyEvent (QKeyEvent)
import qualified Graphics.UI.Qtah.Gui.QKeyEvent as QKeyEvent
import           Graphics.UI.Qtah.Widgets.QApplication (QApplication)
import qualified Graphics.UI.Qtah.Widgets.QApplication as QApplication
import           Graphics.UI.Qtah.Widgets.QLabel (QLabel)
import qualified Graphics.UI.Qtah.Widgets.QLabel as QLabel
import           Graphics.UI.Qtah.Widgets.QWidget (QWidgetPtr)
import qualified Graphics.UI.Qtah.Widgets.QWidget as QWidget
import           System.Environment (getArgs, getProgName)
import           System.Random (randomRIO)

main :: IO ()
main = do
  progName <- getProgName
  withApp $ \_ -> do
    window <- newWindow progName
    QWidget.showMaximized window
    QCoreApplication.exec

randomChoice :: [a] -> IO a
randomChoice xs = do
  i <- randomRIO (0, length xs - 1)
  pure $ xs !! i

samples :: [String]
samples = map (:[]) letters ++ digraphs where
  vowels = "АЕЁИОУЫЭЮЯ"
  consonants = "БВГДЖЗЙКЛМНПРСТФХЦЧШЩ"
  signs = "ЪЬ"
  letters = vowels ++ consonants ++ signs
  digraphs
    =   prod vowels vowels
    ++  [[x, y] | x <- consonants, y <- vowels, [x, y] `notElem` ["ЕБ", "ЁБ"]]
    ++  prod consonants signs
    ++  [[x, y] | x <- consonants, y <- vowels, [x, y] `notElem` ["ЖЫ", "ШЫ"]]
    -- ++  [ "БЖ", "БЗ", "БЛ", "БН", "БР",
    --       "ВД", "ВЖ", "ВЗ", "ВК", "ВЛ", "ВМ", "ВН", "ВР", "ВС", "ВТ", "ВЦ",
    --       "ВЧ", "ВШ"]
  prod xs ys = [[x, y] | x <- xs, y <- ys]

newWindow :: String -> IO QLabel
newWindow progName = do
  this <- QLabel.new
  fontSize <- newIORef MaxFontSize
  withFont this $ \font -> QFont.setPointSize font MaxFontSize
  QLabel.setAlignment this $ Qt.alignHCenter .|. Qt.alignVCenter
  QWidget.setWindowTitle this progName
  resetSample this
  _ <- onEvent this $ onKeyPress this fontSize
  pure this

onKeyPress :: QLabel -> IORef Int -> QKeyEvent -> IO Bool
onKeyPress this fontSize e = do
  type_ <- QEvent.eventType e
  case type_ of
    QEvent.KeyPress -> do
      key <- QKeyEvent.key e
      case key of
        KeyLeft -> do
          increaseFontSize this fontSize
          resetSample this
          pure True
        KeyRight -> do
          size <- readIORef fontSize
          case size of
            MinFontSize ->
              win this
            _ -> do
              decreaseFontSize this fontSize
              resetSample this
          pure True
        _ ->
          pure False
    _ ->
      pure False

win :: QLabel -> IO ()
win label = do
  withFont label $ \font -> QFont.setPointSize font MaxFontSize
  QLabel.setText label "🌟"

withApp :: (QApplication -> IO a) -> IO a
withApp action = do
  args <- getArgs
  withScopedPtr (QApplication.new args) action

withFont :: QWidgetPtr this => this -> (QFont -> IO a) -> IO ()
withFont this action = do
  x <- toGc =<< QWidget.font this
  _ <- action x
  QWidget.setFont this x

-- withPalette :: QWidgetPtr this => this -> (QPalette -> IO a) -> IO ()
-- withPalette this action = do
--   x <- toGc =<< QWidget.palette this
--   _ <- action x
--   QWidget.setPalette this x

resetSample :: QLabel -> IO ()
resetSample label = do
  sample <- randomChoice samples
  QLabel.setText label sample

pattern KeyLeft :: Int
pattern KeyLeft = 16777234

pattern KeyRight :: Int
pattern KeyRight = 16777236

increaseFontSize :: QLabel -> IORef Int -> IO ()
increaseFontSize label fontSize =
  withFont label $ \font -> do
    size <- readIORef fontSize
    let size' = min (size + fontSizeStep) MaxFontSize
    QFont.setPointSize font size'
    writeIORef fontSize size'

decreaseFontSize :: QLabel -> IORef Int -> IO ()
decreaseFontSize label fontSize =
  withFont label $ \font -> do
    size <- readIORef fontSize
    let size' = max (size - fontSizeStep) MinFontSize
    QFont.setPointSize font size'
    writeIORef fontSize size'

pattern MaxFontSize :: Int
pattern MaxFontSize = 800

pattern MinFontSize :: Int
pattern MinFontSize = 20

fontSizeStep :: Int
fontSizeStep = 150
