{-# LANGUAGE NumericUnderscores #-}

import           Data.Char (isLetter, toUpper)
import qualified Data.Set as Set

-- bigrams :: String -> [String]
-- bigrams [] = []
-- bigrams [a] = []
-- bigrams (a:b:cs) = [a, b] : bigrams (b:cs)

trigrams :: String -> [String]
trigrams [] = []
trigrams [a] = []
trigrams [a, b] = []
trigrams (a:b:c:ds) = [a, b, c] : trigrams (b:c:ds)

main :: IO ()
main =
  interact $
      unlines
    . uniq
    . filter (all isLetter)
    . concatMap (trigrams . map toUpper)
    -- . take 500_000
    . lines

uniq :: [String] -> [String]
uniq = Set.toList . Set.fromList
